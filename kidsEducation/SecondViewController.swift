//
//  SecondViewController.swift
//  OrientationChange
//
//  Created by Rashed on 2017-03-06.
//  Copyright © 2017 Rashed. All rights reserved.
//

import UIKit
import AVFoundation

class SecondViewController: UIViewController,AVAudioPlayerDelegate {

    var rashed : AVAudioPlayer!
    var autoPlayTimer : Timer =  Timer()
    var ASCIIVALUE :Int = Int(65)

    override func viewDidLoad() {
        super.viewDidLoad()

       // autoPlayTimer = Timer.scheduledTimer(timeInterval:1, target:self, selector: #selector(AutoPlayTimerAction), userInfo: nil, repeats: false)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let vl = UIInterfaceOrientation.landscapeLeft.hashValue
        UIDevice.current.setValue(vl, forKey: "orientation")
        
        // let value =  UIInterfaceOrientation.portrait.rawValue
        // UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func AutoPlayTimerAction() {
        
        if rashed != nil{
             rashed.stop()
             rashed = nil
         }
        var string = ""
        string.append(Character(UnicodeScalar(ASCIIVALUE)!))
        onOff(fileName: "\(string).mp3")
        ASCIIVALUE += 1
        
    }
    
    func onOff(fileName:String)     {
        
        let path = Bundle.main.path(forResource: fileName, ofType:nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            rashed = try AVAudioPlayer(contentsOf: url)
            rashed.delegate = self
            rashed.play()
        } catch {
            print("Hi Rashed got error ")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("completed ")
        sleep(1)
        if ASCIIVALUE <= 64+26{
            AutoPlayTimerAction()
        }
    }

}
