//
//  Helper.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-19.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI



private let APP_HEXA_COlOR_CODE : String = "800000"
private let APP_TEXT_HEXA_COlOR_CODE : String = "000000"


@objc class Helper: NSObject {
   
    
     
    @objc static func showAlertMessage(view : UIViewController, title: String, msg : String){
        let alertCon = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        let okbtn = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
        alertCon.addAction(okbtn)
        view.present(alertCon, animated: true, completion: nil)
    }

    
   //
  // MARK: SET NAVIGATION BAR PROPERTY
  //
     @objc
    static func setNavigationBarProperty(navbar:UINavigationController, size: Int ,title:String){
        
        let ssize  = CGFloat(size)
        let attrs = [
            NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font: UIFont(name:getAppTextFontName(), size:ssize)!
         ]
        navbar.navigationBar.titleTextAttributes = attrs
        navbar.navigationBar.topItem?.title = title
        navbar.navigationBar.backgroundColor = getAppColor()
        navbar.navigationBar.barTintColor = getAppColor()
    }
    
    @objc
      static func setTabBarProperty(tabBar:UITabBarController){
        
        tabBar.tabBar.barTintColor = getAppColor()
         tabBar.tabBar.tintColor = UIColor.black
        tabBar.tabBar.unselectedItemTintColor = getTextColor()

       
      }

    
 
   private static  func getHexaStringToColor(hexa: String) -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: hexa))  //
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    
                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
    
      @objc
      static  func getAppColor() -> UIColor {

             // Convert hex string to an integer
             let hexint = Int(self.intFromHexString(hexStr: APP_HEXA_COlOR_CODE))  //
             let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
             let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
             let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
 
             // Create color object, specifying alpha as well
             let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
             return color
         }
      @objc
      static  func getTextColor() -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: APP_TEXT_HEXA_COlOR_CODE))
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
      static func intFromHexString(hexStr: String) -> UInt32 {
             var hexInt: UInt32 = 0
             // Create scanner
             let scanner: Scanner = Scanner(string: hexStr)
             // Tell scanner to skip the # character
             scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
             // Scan hex value
             scanner.scanHexInt32(&hexInt)
             return hexInt
         }
    private static func getAppTextFontName()-> String{
        
       // return "HelveticaNeue-Bold"
         return "MarkerFelt-Wide"
      // return  "Georgia-Bold"
    }
    
    @objc static func getAppName()-> String{
        
      
         return "Children Learning App"
      
    }
      
    @objc  static func SetButtonUIProperty(button: UIButton,title:String,fontSize:Float){
          
          let fontSize = CGFloat(fontSize)
          button.backgroundColor = getAppColor()//  C80000
          if(title != ""){
              button.setTitle(title, for: .normal)
          }
          button.setTitleColor(getTextColor(), for: .normal)
         button.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
         button.layer.cornerRadius = 10
           
      }
    
    
    //
    // MAIL COMPOSE
    //
    @objc
       static func sendEmail(viewCon:UIViewController, fileUrl: URL?,operationType:Int) {
              
              if MFMailComposeViewController.canSendMail() {
                  
                  let mail = MFMailComposeViewController()
                  mail.mailComposeDelegate = viewCon as? MFMailComposeViewControllerDelegate
                 
                  if(operationType == 0){  // action sheet action
                      let attachmentData = NSData(contentsOfFile: fileUrl!.path)
                      let fileName : String = String(fileUrl!.absoluteString.split(separator: "/").last!)
                      let mimetype =  self.MimeTypefrom(filename: fileName)
                      mail.addAttachmentData(attachmentData! as Data, mimeType: mimetype, fileName: fileName)
                      mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                  }
                  else if(operationType == 1) // user feedback and suggestion
                  {
                      mail.setToRecipients(["asma0840500@gmail.com"])
                      mail.title = "User Feedback and Suggestion"
                      mail.setMessageBody("<p>Your Feedback and Suggestions is Our Motivation</p>", isHTML: true)
                  }
                  else if(operationType == 2) // Tell A Friend to share this app
                  {
                      
                      mail.title = "Child Preschool Education"
                      mail.setMessageBody("https://apps.apple.com/us/app/pdf-scanner-ocr/id1511507407", isHTML: true)
                  }
                  
                  viewCon.present(mail, animated: true, completion: {
                      
                      print("test ")
                      
                  })
              } else {
                   
              }
          }
        
       

    
       static func MimeTypefrom(filename:String)-> String{
           
           var mimeType : String = ""
           if ( filename == "m4a" ) {
               mimeType =  "audio/m4a"
           } else if (filename == "png" ) {
               mimeType = "image/png";
           } else if (filename == "doc" ) {
               mimeType = "application/msword";
           } else if (filename == "ppt" ) {
               mimeType = "application/vnd.ms-powerpoint";
           } else if (filename == "html" ) {
               mimeType =  "text/html"
           } else if (filename == "pdf" ) {
               mimeType = "application/pdf";
           }
           
           return mimeType
       }
       
       func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           controller.dismiss(animated: true)
       }
    
    
      
     //
    // MARK: USERDEAFULT PREFERENCE DATA
    //
         @objc
         static func setStudyAutoPlay(onOff:Bool){
             
             let defaults = UserDefaults.standard
             defaults.set(onOff, forKey: "audoPlay")
         }
         @objc
         static func getStudyAutoPlay()->Bool{
                
              let defaults = UserDefaults.standard
      
             if (defaults.object(forKey: "audoPlay") != nil) {
                 
                 let autoOnOff : Bool =  defaults.object(forKey: "audoPlay") as! Bool
                        return autoOnOff
                }
             return true
         }
    @objc
       static func setAutoSaveDrawingImage(onOff:Bool){
           
           let defaults = UserDefaults.standard
           defaults.set(onOff, forKey: "autosave")
       }
       @objc
       static func getAutoSaveDrawingImage()->Bool{
              
            let defaults = UserDefaults.standard
    
           if (defaults.object(forKey: "autosave") != nil) {
               
               let autoOnOff : Bool =  defaults.object(forKey: "autosave") as! Bool
                      return autoOnOff
              }
           return true
       }
    
    
}
