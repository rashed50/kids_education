//
//  DBCommon.h
//  TestAutoLayout
//
//  Created by Arif_iOS on 2/19/16.
//  Copyright © 2016 W3 IMAC 4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "FMDB.h"

@interface DBCommon : NSObject
+ (int) createTable:(NSString*)createTableQuery;

+(BOOL) insert:(NSString *)query :(NSArray *) info;

+(BOOL) remove:(NSString *)query :(NSArray *) info;

+(BOOL) update:(NSString *)query :(NSArray *) info;
+(NSInteger)insertQueryAndgetLastInsertRowId:(NSString *)query info:(NSArray *)info tableName:(NSString *)tableName columnName:(NSString *)columnName;

//+(FMResultSet*) select:(NSString *) query;

+(BOOL) hasData:(NSString *) query;

//Added By Tamal
+(int)count:(NSString*)query;

+(BOOL)updateTablesForUserIDChange:(NSDictionary*)dict;

+(BOOL)updateTable:(NSString*)tableName oldId:(NSString*)old_id new_id:(NSString*)new_id column_name:(NSString*)column;

+(BOOL)updateTableOnly:(NSString*)tableName oldId:(NSString*)old_id new_id:(NSString*)new_id column_name:(NSString*)column;


@end
