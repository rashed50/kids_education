//
//  DBManager.m
//  TeeTimes
//
//  Created by w3Admin on 1/30/14.
//  Copyright (c) 2014 TeeTimes.Net. All rights reserved.
//

#import "DBManager.h"
#import "FMDB.h"
#import "FMDatabaseQueue.h"
#import "DBCommon.h"
 


static NSString *databasePath;
static NSString *databaseName = @"yo.db";
static sqlite3 *yoDatabase;
static DBManager *_instanceDBManager;


// Noman(10.02.2016): GROUPS table info
static NSString *TABLE_YO_GROUPS = @"yo_groups";
static NSString *COLUMN_GROUPS_ID = @"_groupID";
static NSString *COLUMN_GROUPS_NAME = @"_groupName";
static NSString *COLUMN_GROUPS_CREATE_TIME = @"_groupTimeStamp";
static NSString *COLUMN_GROUPS_ADMIN = @"_groupAdmin";

// Noman(10.02.2016): GROUPS_USERS table info
static NSString *TABLE_YO_GROUPS_USERS = @"yo_groupsUsers";
static NSString *COLUMN_GROUP_ID = @"_groupID";
static NSString *COLUMN_FRIENDS_USER_ID = @"friends_user_id";
static NSString *COLUMN_GROUPS_USERS_ADDING_TIME = @"_groupUserAddingTime";

@implementation DBManager


+ (FMDatabaseQueue *)sharedManager
{
     static FMDatabaseQueue *sharedMyManager = nil;
     static dispatch_once_t onceToken;
     dispatch_once(&onceToken, ^{
          sharedMyManager = [[self alloc] init];
     });
     return sharedMyManager;
}

- (id)init
{
     self = [super init];
     if (self) {
          _databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[self databasePath]];
     }
     return self;
}

/**
 *++
 * Anindya(18/06/2015):- Get Database path.
 *
 **/
- (NSString *)databasePath
{
    NSString *docsPath = DIRECTORY_BASE_PATH;
     return [docsPath stringByAppendingPathComponent:databaseName];
}
/*
 *--
 * Anindya(18/06/2015):- Get Database path.
 *
 */

+(DBManager *)sharedInstance
{
     if (_instanceDBManager == nil)
     {
          if([self openDatabaseConnection]!=NULL)
          {
               
               _instanceDBManager = [[self alloc]init];
               _instanceDBManager.yoDatabase1=yoDatabase;
               _instanceDBManager.databaseName1=databaseName;
               _instanceDBManager.databasePath1=databasePath;
               [self sharedManager];
          }
          else
          {
               _instanceDBManager = NULL;
               NSLog(@"Database connection  Error ");
          }
     }
     return _instanceDBManager;
}

/**
 *++
 * Anindya(18/06/2015):- Open Database connection.
 *
 **/

+(sqlite3*)openDatabaseConnection
{

     NSString *docsDir = DIRECTORY_BASE_PATH;
     databasePath= [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:databaseName]];
//     NSLog(@"===== ==== ===== ===== ==== %@ ",databasePath);
     
     const char *dbpath = [databasePath UTF8String];
     if (yoDatabase==NULL)
     {
          if (sqlite3_open(dbpath, &yoDatabase) == SQLITE_OK)
          {
               return  yoDatabase;
          }
          else
               return NULL;
     }
     else
     {
          NSLog(@"  database  connection already open ");
          return yoDatabase;
     }
     return yoDatabase;
     
}



+(int)closeDatabaseConnection
{
     DBManager *manager=[[DBManager alloc]init];
     manager=[DBManager sharedInstance];
     // sqlite3_close(manager.yoDatabase1);
     NSLog(@"Database connection  Close ");
     return 1;
}

+ (void)createDatabaseForFirstTime
{
    
    
//    int   res =  (int)[DBActivityN createDBActivityTable];
//    
//    if (res == 1) {
//        NSLog(@"All database created");
//        [PreferanceHandler setDatabaseStatus:@"YES"];
//        [PreferanceHandler setUpdateDatabaseStatus:@"updated"];
//    }
//    else
//    {
//        NSLog(@"Table Activity not created");
//    }   //   [DBActivityContentNew createDBActivityContentTable];
    
    //     [PreferanceHandler setTablesAddedForVersion320overPrevious:YES];
}


@end

