//
//  DBCommon.m
//  TestAutoLayout
//
//  Created by Arif_iOS on 2/19/16.
//  Copyright © 2016 W3 IMAC 4. All rights reserved.
//

#import "DBCommon.h"
#import "DBManager.h"
//#import "TestAutoLayout-Swift.h"

@implementation DBCommon


+ (int) createTable:(NSString*)createTableQuery
{
    DBManager *manager=[DBManager sharedInstance];
    int statusOfAddingToDB = 0;
    char *errMsg;
    
    const char *sql_stmt = [createTableQuery UTF8String];
    
    if (sqlite3_exec(manager.yoDatabase1, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
    {
        statusOfAddingToDB = 1;
    }
    return statusOfAddingToDB;
}

+(BOOL) execute:(NSString *)query :(NSArray *) info
{
    __block BOOL result = NO;
    DBManager *manager=[DBManager sharedInstance];
    [manager.databaseQueue inDatabase:^(FMDatabase *db) {
        
        if ([db open])
        {
            result =  [db executeUpdate:query withArgumentsInArray:info];
            [db close];
        }
        else
        {
            NSLog(@"DB Error: \(db.lastErrorMessage())");
        }
    }];
    return result;
}

+(BOOL) insert:(NSString *)query :(NSArray *) info{
    return [self execute:query :info];
}

+(BOOL) remove:(NSString *)query :(NSArray *) info
{
    return [self execute:query :info];
}

+(BOOL) update:(NSString *)query :(NSArray *) info
{
    return [self execute:query :info];
}


+(NSInteger)insertQueryAndgetLastInsertRowId:(NSString *)query info:(NSArray *)info tableName:(NSString *)tableName columnName:(NSString *)columnName
{
     BOOL success = false;
     success = [self insert:query :info];
     DBManager *manager = [DBManager sharedInstance];
     __block NSInteger lastRowID = 0;
     if (success) {
          [manager.databaseQueue inDatabase:^(FMDatabase *db) {
               
               if ([db open])
               {
                    NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC LIMIT 1",
                                          columnName,
                                          tableName,
                                          columnName];
                    
                    FMResultSet *result =  [db executeQuery:querySQL];
                    
                    while([result next] == true) {
                         lastRowID = [[result stringForColumn:columnName] integerValue];
                    }
//                    NSLog(@"last row id %ld",(long)lastRowID);
                    
               }
          }];
     }
     return lastRowID;

}

+(BOOL) hasData:(NSString *) query
{
    DBManager *manager = [DBManager sharedInstance];
    __block FMResultSet *result;
    __block BOOL hasData = NO;
    [manager.databaseQueue inDatabase:^(FMDatabase *db) {
        if ([db open])
        {
            result = [db executeQuery:query];
            
            while([result next])
            {
                hasData = YES;
                break;
            }
            
            [result close];
            [db close];
        }
        else {
            NSLog(@"DB Error: \(db.lastErrorMessage())");
        }
    }];
    return hasData;
}


+(int)count:(NSString*)query{
    
    DBManager *manager = [DBManager sharedInstance];
    __block int count = 0;
    [manager.databaseQueue inDatabase:^(FMDatabase *db) {
        if ([db open])
        {
            count = [db intForQuery: query];
            [db close];
        }
        else {
            NSLog(@"DB Error: \(db.lastErrorMessage())");
        }
        
    }];
    return count;
}




@end
