//
//  DBManager.h
//  TeeTimes
//
//  Created by w3Admin on 1/30/14.
//  Copyright (c) 2014 TeeTimes.Net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "FMDatabaseQueue.h"


#define DIRECTORY_BASE_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]

@interface DBManager : NSObject
{
//      sqlite3 *yoDatabase1;
//      sqlite3_stmt *statement1;
//      NSString *databasePath1;
//      NSString *databaseName1;
    
}
@property(nonatomic ,assign) sqlite3 *yoDatabase1;
@property(nonatomic ,assign) sqlite3_stmt *statement1;
@property(nonatomic ,retain) NSString *databaseName1;
@property(nonatomic ,retain) NSString *databasePath1;
@property (nonatomic, strong) FMDatabaseQueue *databaseQueue;


+ (FMDatabaseQueue *)sharedManager;
+ (DBManager *)sharedInstance;
+ (sqlite3*)openDatabaseConnection;
+ (int)closeDatabaseConnection;
+ (void)createDatabaseForFirstTime;

@end
