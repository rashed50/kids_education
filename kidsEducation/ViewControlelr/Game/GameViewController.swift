//
//  GameViewController.swift
//  HitAndScore
//
//  Created by Vamshi Krishna on 26/01/18.
//  Copyright © 2018 Vamshi Krishna. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GoogleMobileAds

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
          }
       
        addBannerToView()
        self.view.backgroundColor = Helper.getAppColor()
        setNavigationBarBackButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.isNavigationBarHidden = true
    }
    
    func addBannerToView() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.adMobBannerView.adSize = kGADAdSizeBanner
        appDelegate.adMobBannerView.rootViewController = self
        appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                          y: view.frame.height - appDelegate.adMobBannerView.frame.height,
                          width: view.frame.width,
                          height: appDelegate.adMobBannerView.frame.height)
        self.view.addSubview(appDelegate.adMobBannerView)
    }
 
    func setNavigationBarBackButton() {

             // self.navigationItem.setHidesBackButton(true, animated:false)
              let view = UIView(frame: CGRect(x: 20, y: 40, width: 40, height: 40))
              let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
              if let imgBackArrow = UIImage(named: "back-arrow-png") {
                  imageView.image = imgBackArrow
              }
              view.addSubview(imageView)
              let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
              view.addGestureRecognizer(backTap)
              self.view.addSubview(view)
        }

    @objc func backToMainViewController() {
           
            let transition = CATransition()
            transition.duration = 0.4
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
    
               
        }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}

