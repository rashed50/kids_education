//
//  ColorPickerViewController.h
//  QR Code P2
//
//  Created by SoftKnight on 1/16/16.
//  Copyright © 2016 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PIDrawerViewController.h"

@protocol ColorPickerDelegate <NSObject>

-(void) completeColorPic: (UIColor *)chosenColor;

@end

@interface ColorPickerViewController : UIViewController

@property (nonatomic, weak) id<ColorPickerDelegate> colordelegate;
//@property (nonatomic,weak) PIDrawerViewController* baseViewController;
@end
