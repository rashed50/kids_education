//
//  ColorPickerViewController.m
//  QR Code P2
//
//  Created by SoftKnight on 1/16/16.
//  Copyright © 2016 SoftKnight. All rights reserved.
//

#import "ColorPickerViewController.h"
#import "DTColorPickerImageView.h"


@interface ColorPickerViewController () <DTColorPickerImageViewDelegate>
{
    UIColor *choosColor;
}

@property (weak) IBOutlet UIView *colorPreviewView;

@end

@implementation ColorPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imageView:(DTColorPickerImageView *)imageView didPickColorWithColor:(UIColor *)color
{
    [self.colorPreviewView setBackgroundColor:color];
    
    CGFloat red, green, blue;
    [color getRed:&red green:&green blue:&blue alpha:NULL];
    
    NSLog(@"Picked Color Components: %.0f, %.0f, %.0f", red * 255.0f, green * 255.0f, blue * 255.0f);
     choosColor = color;
    
}

- (IBAction)ChooseColorAction:(id)sender {
    
     NSLog(@"delegate  %@",self.colordelegate);
     [self.colordelegate completeColorPic:choosColor];
     [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
