//
//  MainViewController.swift
//  kidsEducation
//
//  Created by Rashedul Hoque on 31/5/20.
//  Copyright © 2020 education. All rights reserved.
//

import UIKit
import GoogleMobileAds
class MainViewController: UIViewController {

     //  let bannerAddUitTestID = "ca-app-pub-3940256099942544/2934735716"
     // let bannerAddUitLiveID = "ca-app-pub-7072588706544361/6524993940"
     //  var bannerView: GADBannerView!
    
    @IBOutlet weak var drawingButton: UIButton!
    @IBOutlet weak var kidsLearningButton: UIButton!
    @IBOutlet weak var kidsGameButton: UIButton!
    
    @IBOutlet weak var settingButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        Helper.SetButtonUIProperty(button: drawingButton, title: "", fontSize: 25)
        Helper.SetButtonUIProperty(button: kidsLearningButton, title: "", fontSize: 25)
        Helper.SetButtonUIProperty(button: kidsGameButton, title: "", fontSize: 25)
        Helper.SetButtonUIProperty(button: settingButton, title: "", fontSize: 25)
 
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.isNavigationBarHidden = false
           addBannerToView()
           Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 20, title: "Children Learning & Drawing")
       }
   

    func addBannerToView() {
          let appDelegate = UIApplication.shared.delegate as! AppDelegate
          appDelegate.adMobBannerView.adSize = kGADAdSizeBanner
          appDelegate.adMobBannerView.rootViewController = self
          appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                            y: view.frame.height - appDelegate.adMobBannerView.frame.height,
                            width: view.frame.width,
                            height: appDelegate.adMobBannerView.frame.height)
          self.view.addSubview(appDelegate.adMobBannerView)
      }
    
   

}
