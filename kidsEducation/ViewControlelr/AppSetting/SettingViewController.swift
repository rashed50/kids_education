//
//  ttbleViewController.swift
//  kidsEducation
//
//  Created by Rashedul Hoque on 5/6/20.
//  Copyright © 2020 education. All rights reserved.
//

import UIKit
import MessageUI
import GoogleMobileAds

class SettingViewController: UITableViewController {

    
    @IBOutlet weak var studyAutoPlaySwitch1: UISwitch!
    @IBOutlet weak var autoSaveDrawingImage: UISwitch!
    
    @IBAction func studyAutoPlayAction(_ sender: Any) {
        let st = sender as! UISwitch
        Helper.setStudyAutoPlay(onOff: st.isOn)
    }
    
    @IBAction func autoSaveDrawingImageAction(_ sender: Any) {
        let st = sender as! UISwitch
        Helper.setAutoSaveDrawingImage(onOff: st.isOn)
    }
    
    @IBAction func feedbackAndSuggestionAction(_ sender: Any) {
         
       Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 1)
    }
    
    
    @IBAction func tellFriendAction(_ sender: Any) {
        
       Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 2)
    }
    
    @IBAction func developerAppsAction(_ sender: Any) {
        
        let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
        var viewCon : UIViewController
        if #available(iOS 13.0, *) {
              viewCon = story.instantiateViewController(identifier: "pageContainerViewController")
        } else {
           viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
        }
        
        self.navigationController?.pushViewController(viewCon, animated: true)
         
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       addBannerToView()

       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:"App Settings")
         setNavigationBarBackButton()
         autoSaveDrawingImage.isOn = Helper.getAutoSaveDrawingImage()
         studyAutoPlaySwitch1.isOn = Helper.getStudyAutoPlay()
          
      }

    // MARK: - Table view data source
 /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func addBannerToView() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.adMobBannerView.adSize = kGADAdSizeBanner
        appDelegate.adMobBannerView.rootViewController = self
        appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                          y: view.frame.height - appDelegate.adMobBannerView.frame.height,
                          width: view.frame.width,
                          height: appDelegate.adMobBannerView.frame.height)
        self.view.addSubview(appDelegate.adMobBannerView)
    }
    func setNavigationBarBackButton() {

          
        
        
        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

        if let imgBackArrow = UIImage(named: "back-arrow-png") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
        }

    @objc func backToMainViewController() {
           
            let transition = CATransition()
            transition.duration = 0.4
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
    
               
        }
    
    

  
       
       
       
}


extension SettingViewController : MFMailComposeViewControllerDelegate{
   
     public func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult,
                                      error: Error?) {
        switch (result) {
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        case .sent:
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            controller.dismiss(animated: true, completion: {
               
                let sendMailErrorAlert = UIAlertController.init(title: "Failed",
                                                                message: "Unable to send email. Please check your email and Internet Connection " +
                    "settings and try again.", preferredStyle: .alert)
                sendMailErrorAlert.addAction(UIAlertAction.init(title: "OK",
                                                                style: .default, handler: nil))
                controller.present(sendMailErrorAlert, animated: true, completion: nil)
            })
        default:
            break;
        }
     }

}
