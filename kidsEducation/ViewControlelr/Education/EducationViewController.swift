//
//  ViewController.swift
//  CollectionView
//
//  Created by Mostafizur Rahman on 3/5/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileAds

class EducationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,GADRewardedAdDelegate {

    var rewardedAd: GADRewardedAd?
    var selectedCategoryToOpen = 0
    @IBOutlet var collectionView: UICollectionView!

    var allCatetoryList :[Dictionary<String,String>] = []
    var allCategoryListArray :NSMutableArray = NSMutableArray()
    let dataArray = ["alphabets.png", "animals.png", "bathroom.png", "birds.png", "bodyparts.png", "color.png", "day.png", "dress.png", "electronics.png","fish.png","flowers.png","fruits.png","furniture.png","insects.png","kitchen.png","maps.png","month.png","numbers.png","shapes.png","solarsystem.png","sports.png","vegetables.png","vehicles.png","vitamin.png","mathsign.png"]

    //"write.png",
    override func viewDidLoad() {
        super.viewDidLoad()
         addBannerToView()
         readJSonData()
         addRewardAdToView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
       
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Learning Details")
        setNavigationBarBackButton()
        setDeviceOrintation()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Learning Details"
    }
    
    func addRewardAdToView(){
        
        rewardedAd = GADRewardedAd(adUnitID: "ca-app-pub-3940256099942544/1712485313")
        rewardedAd?.load(GADRequest()) { error in
          if let error = error {
            // Handle ad failed to load case.
          } else {
            // Ad successfully loaded.
          }
        }
    }
    
    func openRewardAdd() {
        
         if rewardedAd?.isReady == true {
            rewardedAd?.present(fromRootViewController: self, delegate:self)
         }
    }
    
    func openDetailsView() {
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let vc = storyboard.instantiateViewController(withIdentifier: "details") as! Details
              vc.selectedCatetoryList = self.generateSelectedCategoryList(list: allCategoryListArray, selectedCatId: selectedCategoryToOpen)
              self.navigationController?.pushViewController(vc,animated: true)
    }
    
    /// MARK:-   Delegate Method of   Reward Ad
    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
      print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }
    /// Tells the delegate that the rewarded ad was presented.
    func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
      print("Rewarded ad presented.")
    }
    /// Tells the delegate that the rewarded ad was dismissed.
    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
      print("Rewarded ad dismissed.")
        openDetailsView()
        
    }
    /// Tells the delegate that the rewarded ad failed to present.
    func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
       print("Rewarded ad failed to present.")
       openDetailsView()
    }
    
    
    
    func addBannerToView() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.adMobBannerView.adSize = kGADAdSizeBanner
        appDelegate.adMobBannerView.rootViewController = self
        appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                          y: view.frame.height - appDelegate.adMobBannerView.frame.height,
                          width: view.frame.width,
                          height: appDelegate.adMobBannerView.frame.height)
        self.view.addSubview(appDelegate.adMobBannerView)
    }
    
    func setNavigationBarBackButton() {

              self.navigationItem.setHidesBackButton(true, animated:false)
              let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
              let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
              if let imgBackArrow = UIImage(named: "back-arrow-png") {
                  imageView.image = imgBackArrow
              }
              view.addSubview(imageView)
              let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
              view.addGestureRecognizer(backTap)
              let leftBarButtonItem = UIBarButtonItem(customView: view)
              self.navigationItem.leftBarButtonItem = leftBarButtonItem
          }

          @objc func backToMainViewController() {
                     let transition = CATransition()
                     transition.duration = 0.4
                     transition.type = CATransitionType.push
                     transition.subtype = CATransitionSubtype.fromRight
                     self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                     self.navigationController?.popViewController(animated: false)
               
           }
    
    func setDeviceOrintation() {
        let vl = UIInterfaceOrientation.portrait.hashValue
        let currentOr = UIDevice.current.value(forKey: "orientation") as! Int
        if   currentOr != vl  {
            UIDevice.current.setValue(vl, forKey: "orientation")
            print("orientation has changed")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
           
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
//    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        return UIInterfaceOrientationMask.portrait
//    }
    private func shouldAutorotate() -> Bool {
        return true
    }
    
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! CollectionCell
        
        cell.itemImageView.image = UIImage(named:dataArray[indexPath.row])
        return cell
        
    }
   

    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      //  if indexPath.row == 0 {
            
          //  openDrawingView()
       // }else{
           /*
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "details") as! Details
            vc.selectedCatetoryList = self.generateSelectedCategoryList(list: allCategoryListArray, selectedCatId: indexPath.row)
            self.navigationController?.pushViewController(vc,animated: true)
           */
        selectedCategoryToOpen = indexPath.row
        openRewardAdd()
       // }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


extension EducationViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets()
    }
    
    fileprivate var itemsPerRow: CGFloat {
        
        //com.education.KidsEdu
        print(UIDevice.current.name)
        let deviceName : String = UIDevice.current.model
        if deviceName.contains("iPhone")
        {
            return 4
        }else if deviceName.contains("iPod")
        {
            return 2
        }else if deviceName.contains("iPad")
        {
            return 6
        }
        return 2
    }
    
    fileprivate var interItemSpace: CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let availableWidth = collectionView.bounds.width - (itemsPerRow + 1)*interItemSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: interItemSpace, left: interItemSpace, bottom: interItemSpace, right: interItemSpace)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interItemSpace
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interItemSpace
    }
    
    
    func readJSonData(){
    
        let filename : String = "gameJsonData"
        
        if let path = Bundle.main.path(forResource: filename, ofType: "txt") {
            do {
                let text = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                convertToDictionary(text: text)
            } catch {
                print("Failed to read text from \(filename)")
            }
        } else {
            print("Failed to load file from app bundle \(filename)")
        }
    
    
    
    }
    
    
    func convertToDictionary(text: String) {
        
        let data1: Data = text.data(using: .utf8, allowLossyConversion: true)!
 
            do {
                let jsonList = try JSONSerialization.jsonObject(with: data1, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                allCategoryListArray = jsonList
            
            }
            catch {
              print("error ocurred for parsing data")
            }
    }
    
    
    
    
    func generateSelectedCategoryList(list: NSMutableArray , selectedCatId: Int) -> [Dictionary<Int,String>] {
        
        
        var selectedCatetoryList :[Dictionary<Int,String>] = []
         print("selected id ",selectedCatId)
        
         for counter in 0 ..< list.count {
           
            let listDic = list[counter] as AnyObject
            let categoryId : Int = (listDic["category_id"] as? Int)!
            let soundLink : String = (listDic["image_sound"] as? String)!
            let imageLink : String = (listDic["image_link"] as? String)!
            let imageName : String = (listDic["image_name"] as? String)!
            
            if categoryId == selectedCatId {
                let dict = [3 : soundLink, 4 : imageLink ,5 : imageName] as [Int : String]
                selectedCatetoryList.append(dict)
            }
        }
        
        return selectedCatetoryList;
    }
    
    
    func openDrawingView() {
        
        let dv : PIDrawerViewController = (UIStoryboard.init(name: "Main", bundle: nil)).instantiateViewController(withIdentifier: "pIDrawerViewController") as! PIDrawerViewController
        self.navigationController?.pushViewController(dv, animated: true)
         
    }
    


}

