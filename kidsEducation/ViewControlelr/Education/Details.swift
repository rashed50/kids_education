//
//  Details.swift
//  CollectionView
//
//  Created by Mostafizur Rahman on 3/5/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileAds

class Details: UIViewController,AVAudioPlayerDelegate {
    
    
    
    var selectedCatetoryList :[Dictionary<Int,String>] = []
    let categoryImage : UIImage = UIImage()
    let categoryName :String = String()
    
    var rashed : AVAudioPlayer!
    var soundMuted : Bool = true
    var isAutoPlayOn = false
    
    var autoPlayTimer : Timer =  Timer()
    var ASCIIVALUE :Int = Int(-1)
  
    
    
    @IBOutlet var soundButtonOutlet: UIButton!
    @IBOutlet var playPauseButtonOutlet: UIButton!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var imageHeight: NSLayoutConstraint!
    var imageHeightNew : CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
       // addBannerToView()
        imageView.image = nil
       // let session = AVAudioSession.sharedInstance()
       // try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSession.CategoryOptions.defaultToSpeaker)
 
        
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            } else {
                // Fallback on earlier versions
            }
            try AVAudioSession.sharedInstance().setActive(true)
       
        } catch {
            print(error)
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        imageHeightNew = UIScreen.main.bounds.size.width-40
        imageHeight.constant = imageHeightNew
        self.navigationController?.isNavigationBarHidden = true
        setDeviceOrintation()
        ASCIIVALUE = 0
        isAutoPlayOn = Helper.getStudyAutoPlay()
        if(isAutoPlayOn){
            playPauseButtonOutlet.setImage(UIImage.init(named: "slideshowon.png"), for:.normal)
        }
     
    }
    
    
    func addBannerToView() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.adMobBannerView.adSize = kGADAdSizeBanner
        appDelegate.adMobBannerView.rootViewController = self
        appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                          y: view.frame.height - appDelegate.adMobBannerView.frame.height,
                          width: view.frame.width,
                          height: appDelegate.adMobBannerView.frame.height)
        self.view.addSubview(appDelegate.adMobBannerView)
    }
    func setDeviceOrintation() {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        let currentOr = UIDevice.current.value(forKey: "orientation") as! Int
        if   currentOr != value  {
            UIDevice.current.setValue(value, forKey: "orientation")
            print("orientation has changed")
        }
 // addBannerToView()
    }
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated);
        setDeviceOrintation()
        prepareInformationForPlaying()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
 
    private func shouldAutorotate() -> Bool {
        return false
    }
 
    
    @IBAction func homeButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
      
    }
    
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if rashed == nil {
            initializeForCategoryOfAlPhabet()
        }
        else if (rashed.isPlaying) {
          
            isAutoPlayOn = false
            rashed.pause()
            playPauseButtonOutlet.setImage(UIImage.init(named: "slideshowoff.png"), for:.normal)
        }
        else{
            if !isAutoPlayOn{
                AutoPlayTimerAction()
            }else {
                
                isAutoPlayOn = true
                rashed.play()
            }
            playPauseButtonOutlet.setImage(UIImage.init(named: "slideshowon.png"), for:.normal)

        }
        playPauseButtonOutlet.contentMode = UIView.ContentMode.scaleAspectFit
        playPauseButtonOutlet.clipsToBounds = true;
        
    }
    
    @IBAction func soundButtonAction(_ sender: Any) {
      
        if rashed != nil {
           soundMuted = !soundMuted
           speakerOnOff()
        }
    }
    
    @IBAction func rightArrowButtonAction(_ sender: Any) {
        
        print("current value is %d" , ASCIIVALUE)
        ASCIIVALUE += 1;
        if ASCIIVALUE >= selectedCatetoryList.count
        {
            ASCIIVALUE = 0
           
        }
        prepareInformationForPlaying()
    }
    
    
    @IBAction func lefttArrowButtonAction(_ sender: Any) {
        
        print("current value is %d" , ASCIIVALUE)
        ASCIIVALUE -= 1;
        if ASCIIVALUE <= -1
        {
            ASCIIVALUE = 0
        }
        
        prepareInformationForPlaying()
    }
    
    func initializeForCategoryOfAlPhabet() {
        autoPlayTimer = Timer.scheduledTimer(timeInterval:0.2, target:self, selector: #selector(AutoPlayTimerAction), userInfo: nil, repeats: false)
    }
    
    func speakerOnOff()
    {
        if soundMuted
        {
            rashed.volume = 1
            soundButtonOutlet.setImage(UIImage.init(named: "speaker"), for:.normal)
        }
        else {
            rashed.volume = 0
            soundButtonOutlet.setImage(UIImage.init(named: "speaker_off"), for:.normal)
        }
        soundButtonOutlet.contentMode = UIView.ContentMode.scaleAspectFit
        soundButtonOutlet.clipsToBounds = true
        
    }
    
    func destroyPlayer() {
    }
    @objc func AutoPlayTimerAction() {
        
        isAutoPlayOn = true;
        ASCIIVALUE += 1;
        if ASCIIVALUE <= -1 || ASCIIVALUE >= selectedCatetoryList.count
        {
            ASCIIVALUE = 0;
        }
        prepareInformationForPlaying()
        
    }
    
    
    func prepareInformationForPlaying() {
       
        destroyPlayer()
        var imageLink = ""
        var soundLink = ""
        var currentRunningDic : Dictionary<Int,String> = [:]
        currentRunningDic = selectedCatetoryList[ASCIIVALUE] as Dictionary;
        imageLink = currentRunningDic[4]!
        soundLink = currentRunningDic[3]!
        setImage(fileName: "\(imageLink).png" )
        onOff(fileName: "\(soundLink).mp3")
       
    }
    
    func getDocumentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    func setImage(fileName:String) {
        self.imageView.image = UIImage.init(named: fileName)
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.clipsToBounds = true
        print(self.imageView.frame)
    }
    
    func onOff(fileName:String)     {
       
        let basePath = Bundle.main.path(forResource: fileName, ofType:nil)!
        let url = URL(fileURLWithPath: basePath )
    
        do {
            rashed = try AVAudioPlayer(contentsOf: url)
            rashed.delegate = self
            speakerOnOff()
            rashed.play()
        } catch {
            print("Hi Rashed got error ")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("completed ")
        if ASCIIVALUE != 0 {
            sleep(1)
        }
        if ASCIIVALUE < selectedCatetoryList.count
        {
            if isAutoPlayOn{
                 AutoPlayTimerAction()
            }
        }else
        {
          
            ASCIIVALUE = 0
            destroyPlayer()
        }
    }
    
    
}
  
