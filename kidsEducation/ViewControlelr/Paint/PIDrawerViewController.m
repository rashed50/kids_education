//
//  PIDrawerViewController.m
//  PIImageDoodler
//
//  Created by Pavan Itagi on 07/03/14.
//  Copyright (c) 2014 Pavan Itagi. All rights reserved.
//

#import "PIDrawerViewController.h"
#import "PIDrawerView.h"
#import "PIColorPickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "MBProgressHUD.h"

//

@interface PIDrawerViewController () <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, PIColorPickerControllerDelegate>
{
    ColorPickerViewController *colorPicker;

}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *writeButtonWidthConstraint;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UIButton *writeButton;
@property (strong, nonatomic) IBOutlet UIButton *pickEraserButton;

@property (strong, nonatomic) IBOutlet UIButton *pickImageButton;
@property (strong, nonatomic) IBOutlet UIButton *pickColorButton;

@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@property (strong, nonatomic) IBOutlet PIDrawerView *drawerView;
@property (strong, nonatomic) IBOutlet UIImageView *backGroundImageView;


- (IBAction)writeButtonPressed:(id)sender;
- (IBAction)pickImageButtonPressed:(id)sender;
- (IBAction)pickColorButtonPressed:(id)sender;
- (IBAction)pickEraserButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)newPageButtonPressed:(id)sender;


//@property (strong, nonatomic) IBOutlet UIButton *newPageButton;

//@property (weak, nonatomic) IBOutlet PIDrawerView *drawerView;
//@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
//@property (weak, nonatomic) IBOutlet UIButton *pickImageButton;
//@property (weak, nonatomic) IBOutlet UIButton *pickColorButton;
//@property (weak, nonatomic) IBOutlet UIButton *pickEraserButton;
//@property (weak, nonatomic) IBOutlet UIButton *writeButton;
//@property (weak, nonatomic) IBOutlet UIButton *saveButton;
//
@property (nonatomic, strong) UIColor *selectedColor;

//- (IBAction)pickImageButtonPressed:(id)sender;
//- (IBAction)pickColorButtonPressed:(id)sender;
//- (IBAction)pickEraserButtonPressed:(id)sender;
//- (IBAction)saveButtonPressed:(id)sender;
//- (IBAction)writeButtonPressed:(id)sender;




@end

@implementation PIDrawerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = YES;
    [self.pickColorButton setImage:nil forState:UIControlStateNormal];
    self.pickColorButton.layer.cornerRadius =   (self.pickColorButton.frame.size.width /2);
    self.pickColorButton.clipsToBounds = YES;
    [self.pickColorButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.pickColorButton.layer setBorderWidth:5.0];
    
    
    self.selectedColor = [UIColor redColor];
    [self.pickColorButton setBackgroundColor:self.selectedColor];
    [self.drawerView setSelectedColor:self.selectedColor];
    // Do any additional setup after loading the view from its nib.
    self.scrollView.showsHorizontalScrollIndicator = true;
    self.scrollView.alwaysBounceHorizontal = true;
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self writeButtonPressed:nil];

}

-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:animated];
    [self setDeviceOrintation];
    _writeButtonWidthConstraint.constant = 0;
    _writeButton.hidden = YES;
}

- (BOOL)shouldAutorotate {
    return true;
}
//- (NSUInteger)supportedInterfaceOrientations {
//    return (UIInterfaceOrientationMaskLandscapeLeft);
//}

-(BOOL)prefersStatusBarHidden{
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

//#pragma mark - Button Action methods
//- (IBAction)pickImageButtonPressed:(id)sender
//{
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Pick" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Pick From Library",nil];
//    [actionSheet showInView:self.view];
//}
//
//- (IBAction)pickColorButtonPressed:(id)sender
//{
//    PIColorPickerController *colorPicker = [[PIColorPickerController alloc] initWithNibName:@"PIColorPickerController" bundle:nil];
//    [colorPicker setDelegate:self];
//    [colorPicker setCurrentSelectedColor:self.selectedColor];
//    [self.navigationController pushViewController:colorPicker animated:YES];
//}

//- (IBAction)homeBtnAction:(id)sender {
//    
//    [self.navigationController popToRootViewControllerAnimated:YES];
//}

//- (IBAction)newBtnAction:(id)sender {
//}
//
//
//- (IBAction)pickEraserButtonPressed:(id)sender
//{
//    [self.drawerView setDrawingMode:DrawingModeErase];
//}
//
//- (IBAction)saveButtonPressed:(id)sender
//{
//    
//}
//
//- (IBAction)writeButtonPressed:(id)sender
//{
//    [self.drawerView setDrawingMode:DrawingModePaint];
//}



-(void)setDeviceOrintation{
    int value = (int)[NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    int currentOr =  [[[UIDevice currentDevice] valueForKey:@"orientation"] intValue];
    
    if (value != currentOr) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:value] forKey:@"orientation"];
    }else {
      //  [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:value] forKey:@"orientation"];
    }

}
 

- (void)pickPhoto:(NSString*)sourceType
{
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    if ([sourceType isEqualToString:@"Camera"]) {
        mediaUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    mediaUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    mediaUI.allowsEditing = NO;
    mediaUI.delegate = self;
    [self presentViewController:mediaUI animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = nil;
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    else
    {
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo)
        {
            image = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
        }
    }
    
    self.backGroundImageView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PIColorPickerControllerDelegate methods
- (void)colorSelected:(UIColor *)selectedColor
{
    self.selectedColor = selectedColor;
    [self.pickColorButton setBackgroundColor:selectedColor];
    [self.drawerView setSelectedColor:self.selectedColor];
    [self writeButtonPressed:nil];
}

// new

-(void)completeColorPic:(UIColor *)selectedColor
{
  //  UIColor *col = chosenColor;
    
    self.selectedColor = selectedColor;
    [self.pickColorButton setBackgroundColor:selectedColor];
    [self.drawerView setSelectedColor:self.selectedColor];
    [self writeButtonPressed:nil];

}
- (IBAction)homeBtnAction:(id)sender {
     [self.navigationController popViewControllerAnimated:true];
}


- (IBAction)writeButtonPressed:(id)sender {
   [self.drawerView setDrawingMode:DrawingModePaint];
}

- (IBAction)pickImageButtonPressed:(id)sender {
    
 
    
    UIAlertController *alertCont =[UIAlertController alertControllerWithTitle:@"Alert" message:@"Choose options  to pick photo " preferredStyle:UIAlertControllerStyleActionSheet];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [self pickPhoto:@"Camera"];
      }]];
    
    [alertCont addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [self pickPhoto:@"Gallery"];
    }]];
    
    [alertCont addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        
    }]];
    
    [self presentViewController:alertCont animated:YES completion:nil];
    
}

- (IBAction)pickColorButtonPressed:(id)sender {

    
    
       PIColorPickerController *colorPicker1 = [[PIColorPickerController alloc] initWithNibName:@"PIColorPickerController" bundle:nil];
        [colorPicker1 setDelegate:self];
        [colorPicker1 setCurrentSelectedColor:self.selectedColor];
        [self.navigationController pushViewController:colorPicker1 animated:YES];
    
    
    /*
    colorPicker = [[ColorPickerViewController alloc] init];
    colorPicker.colordelegate = (id<ColorPickerDelegate>) self;
    colorPicker = [self.storyboard instantiateViewControllerWithIdentifier:@"colorPicker"];
    NSLog(@"delegate  in %@  %@",colorPicker.colordelegate,self);
    [self presentViewController:colorPicker animated:YES completion:nil ];
    
    */
}

- (IBAction)pickEraserButtonPressed:(id)sender {
    
     [self.drawerView setDrawingMode:DrawingModeErase];
}

- (IBAction)saveButtonPressed:(id)sender {
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UIImage *drawImg = [self imageWithView:_drawerView];
    [self savePhoto:drawImg];
    
    
}

- (IBAction)newPageButtonPressed:(id)sender {
  //  [self.drawerView undo];
    [self.drawerView allClear:self.drawerView.maskView];
   
}


-(void)savePhoto: (UIImage*)image {
   
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)   savedPhotoImage:(UIImage *)image
  didFinishSavingWithError:(NSError *)error
               contextInfo:(void *)contextInfo
{
   
    NSString *msg =@"Image not saved" ,*title = @"Error";
    
    if (error == Nil) {
        title  = @"Success";
        msg = @"This image has been saved to your camera roll";
        [self.drawerView allClear:self.drawerView.maskView];
    }
    
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *handle){
    
    
    }];
    [alertCon addAction:okBtn];
    [self presentViewController:alertCon animated:YES completion:nil];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
@end
