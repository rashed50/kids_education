//
//  PIDrawerViewController.h
//  PIImageDoodler
//
//  Created by Pavan Itagi on 07/03/14.
//  Copyright (c) 2014 Pavan Itagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerViewController.h"

@interface PIDrawerViewController : UIViewController<ColorPickerDelegate>

-(void)completeColorPic:(UIColor *)selectedColor;
- (IBAction)homeBtnAction:(id)sender;



@end
