import Foundation
import UIKit

class  MyPageViewController: UIPageViewController {
    fileprivate var myAllPublishApps: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        decoratePageControl()
        populateItems()
        if let firstViewController = myAllPublishApps.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    fileprivate func decoratePageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [MyPageViewController.self])
        pc.currentPageIndicatorTintColor = .orange
        pc.pageIndicatorTintColor = .gray
    }
    
    fileprivate func populateItems() {
        let appNameDetails = ["PDF Scanner OCR","Add Music with Video","Add Music with Video"]
        
        
        let appStoreLink = ["https://apps.apple.com/us/app/pdf-scanner-ocr/id1511507407",
                            "https://apps.apple.com/us/app/edit-video-by-music/id1512073814",
                             "https://apps.apple.com/us/app/edit-video-by-music/id1512073814",
                           ]
        let backgroundColor:[UIColor] = [.lightGray, .purple, .green]
        
        for (index, t) in appNameDetails.enumerated() {
            let c = createCarouselItemControler(with: t, with: backgroundColor[0],appIconName:t,appStoreLink:appStoreLink[index])
            myAllPublishApps.append(c)
        }
    }
    
    fileprivate func createCarouselItemControler(with titleText: String?, with color: UIColor?, appIconName:String,appStoreLink:String) -> UIViewController {
        let c = UIViewController()
        c.view = ItemView(titleText: titleText, background: color, appIconName:appIconName,appStoreLink:appStoreLink)

        return c
    }
}

// MARK: - DataSource

extension MyPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myAllPublishApps.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return myAllPublishApps.last
        }
        
        guard myAllPublishApps.count > previousIndex else {
            return nil
        }
        
        return myAllPublishApps[previousIndex]
    }
    
    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myAllPublishApps.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        guard myAllPublishApps.count != nextIndex else {
            return myAllPublishApps.first
        }
        
        guard myAllPublishApps.count > nextIndex else {
            return nil
        }
        
        return myAllPublishApps[nextIndex]
    }
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return myAllPublishApps.count
    }
    
    func presentationIndex(for _: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = myAllPublishApps.firstIndex(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
}
