//
//  AppDelegate.swift
//  OrientationChange
//
//  Created by Rashed on 2017-03-06.
//  Copyright © 2017 Rashed. All rights reserved.
//

import UIKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    let bannerAddUitTestID = "ca-app-pub-3940256099942544/2934735716"
    let bannerAddUitLiveID = "ca-app-pub-7072588706544361/6524993940"
    let rewardAddUitLiveID = "ca-app-pub-7072588706544361/6694769930"
    let rewardAddUitTestID = "ca-app-pub-3940256099942544/1712485313"
    
    var adMobBannerView: GADBannerView!
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        initializeGoogleAdmob()
        return true
    }
 
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        let vv = self.window?.rootViewController
        if vv is UINavigationController{
            
            let nav : UINavigationController = vv as! UINavigationController
            if nav.topViewController is Details{
                return .landscape
            }else {
                return .portrait
            }
             
        }
         
        
        /*
        if  let vv = self.window?.rootViewController{
            
            if vv is UINavigationController{
                
                let nav : UINavigationController = vv as! UINavigationController
                if nav.visibleViewController is EducationViewController{
                    return .portrait
                }
                else{
                   // return .landscape
                }
            }
        }
      */
       return .portrait
    }
    
 
    
    
 

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    
    
    
    func initializeGoogleAdmob() {
            
            adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
           adMobBannerView.adUnitID = bannerAddUitLiveID
           adMobBannerView.delegate = self as? GADBannerViewDelegate
           adMobBannerView.load(GADRequest())
           // adMobBannerView.isHidden = true
        
        
        
    }
    

}

extension GADBannerViewDelegate{
   
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
    // addBannerViewToView(bannerView)
     //   bannerView.isHidden = false
      print("adViewDidReceiveAd")
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
          bannerView.alpha = 1
        })
        
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
    
}
